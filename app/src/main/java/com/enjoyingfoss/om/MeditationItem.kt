/*
 *     Copyright (c) 2017 Miroslav Mazel.
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.enjoyingfoss.om

import android.os.Parcel
import android.os.Parcelable

/**
@author Miroslav Mazel
 */
data class MeditationItem (
        val mediaFileRaw: Int,
        val titleString: Int,
        val author: MeditationAuthor,
        val license: MeditationLicense,
        val source: MeditationSource,
        val changesString: Int?
) : Parcelable {
    companion object {
        @JvmField val CREATOR: Parcelable.Creator<MeditationItem> = object : Parcelable.Creator<MeditationItem> {
            override fun createFromParcel(source: Parcel): MeditationItem = MeditationItem(source)
            override fun newArray(size: Int): Array<MeditationItem?> = arrayOfNulls(size)
        }
    }

    constructor(source: Parcel) : this(
    source.readInt(),
    source.readInt(),
    source.readParcelable<MeditationAuthor>(MeditationAuthor::class.java.classLoader),
    source.readParcelable<MeditationLicense>(MeditationLicense::class.java.classLoader),
    source.readParcelable<MeditationSource>(MeditationSource::class.java.classLoader),
    source.readValue(Int::class.java.classLoader) as Int?
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) {
        dest.writeInt(mediaFileRaw)
        dest.writeInt(titleString)
        dest.writeParcelable(author, 0)
        dest.writeParcelable(license, 0)
        dest.writeParcelable(source, 0)
        dest.writeValue(changesString)
    }
}