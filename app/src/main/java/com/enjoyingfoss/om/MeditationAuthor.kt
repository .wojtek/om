/*
 *     Copyright (c) 2017 Miroslav Mazel.
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.enjoyingfoss.om

import android.os.Parcel
import android.os.Parcelable

/**
@author Miroslav Mazel
 */
class MeditationAuthor(
        val titleId: Int,
        val linkId: Int?,
        val imageId: Int?
) : Parcelable {
    companion object {
        @JvmField val CREATOR: Parcelable.Creator<MeditationAuthor> = object : Parcelable.Creator<MeditationAuthor> {
            override fun createFromParcel(source: Parcel): MeditationAuthor = MeditationAuthor(source)
            override fun newArray(size: Int): Array<MeditationAuthor?> = arrayOfNulls(size)
        }
    }

    constructor(source: Parcel) : this(
    source.readInt(),
    source.readValue(Int::class.java.classLoader) as Int?,
    source.readValue(Int::class.java.classLoader) as Int?
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) {
        dest.writeInt(titleId)
        dest.writeValue(linkId)
        dest.writeValue(imageId)
    }
}