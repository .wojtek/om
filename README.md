Om
---

About
=====

The goal for this app is to increase emotional intelligence worldwide, and thereby increase global well-being.

The two mindfulness meditations included serve as a starting point.

Volunteering
============

If you'd like to get involved, **send a message on [Gitter](https://gitter.im/om-meditation/Lobby)**.

I'd love help with translations, script preparation, voice acting, user research, user testing, development, spreading the word, or anything else that would help the app.

Donation
========
If you'd prefer to donate, you can use the following:
* **[Liberapay](https://liberapay.com/Om/)**
* [BountySource](https://salt.bountysource.com/teams/om-app)

License
=======
Code is released under the GPLv3 and later license (full text in the GPLv3 file), graphics under the [CreativeCommons Attribution-ShareAlike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/legalcode) license. Each audio file used within the app has a dedicated attribution screen within the app.

Download
=======
[<img src="https://f-droid.org/badge/get-it-on.png"
      alt="Get it on F-Droid"
      height="80">](https://f-droid.org/packages/com.enjoyingfoss.om/)
[<img src="https://play.google.com/intl/en_us/badges/images/generic/en-play-badge.png"
      alt="Get it on Google Play"
      height="80">](https://play.google.com/store/apps/details?id=com.enjoyingfoss.om)
